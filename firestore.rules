rules_version = '2';
service cloud.firestore {
  match /databases/{database}/documents {
    // first rule counts, so doesn't matter what comes later, everyone can do everything
    // match /{document=**} {
    //   allow read, write;
    // }

    function isLoggedIn() {
        return request.auth.uid != null;
    }

    match /persons/{userId} {
        allow read, create: if isLoggedIn();
        allow update, delete: if request.auth.uid == userId;
    }

    // eventuell subcollection für private Daten erstellen
    match /persons/{userId}/private/{docId} {
        allow read, write: if request.auth.uid == userId;
    }

    match /persons/{userId}/rating/{ratingId} {
        allow read, create: if isLoggedIn();
    }

     match /persons/{userId}/address/{addressId} {
        allow read: if isLoggedIn();
        allow write: if request.auth.uid == userId;
    }

    match /orders/{orderId} {
        allow read, create: if isLoggedIn();
    }

    function isHelper(orderId) {
        return get(/databases/$(database)/documents/orders/$(orderId)).data.helper.uid == request.auth.uid;
    }

    function isCreator(orderId) {
        return get(/databases/$(database)/documents/orders/$(orderId)).data.creator.uid == request.auth.uid;
    }

    // eventuell subcollection für Bearbeiter erstellen
    // helper muss bei Order Erstellung auf null gesetzt werden 
    match /orders/{orderId} {
        allow update: if isCreator(orderId) || resource.data.helper == null || isHelper(orderId);
        allow delete: if isCreator(orderId)
    }

    match /orders/{orderId}/product/{productId} {
        allow read, create: if isLoggedIn();
        allow update, delete:if isCreator(orderId)
    }

    match /orders/{orderId}/bill/{docId} {
        allow write: if isHelper(orderId);
    }

    match /orders/{orderId}/bill/{docId} {
        allow read: if isCreator(orderId) || isHelper(orderId);
    }
  }
}