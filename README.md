# nearbuy

An application to strengthen the relationships in your neighbourhood.

## Getting Started

### Run the app in dev mode

This project uses Flutter and Dart. To run this project you need to have [Flutter](https://flutter.dev/docs/get-started/install) installed. 

This project uses Firebase Auth for Authentication, Firebase Firestore as database and cloud functions from Firebase to update the database in some cases.

To start this project in dev mode you have to contact the developers, that you android debug key can be added to the firebase project.

If you have flutter installed, set up an android phone and added you debug key you can start the app:

```
flutter run
```

### build the project 

To build the apk run: 

```
flutter build apk
```

The functionality to run the app on an iOS device is not configured.

### Install the apk

You can find the built apk in ```build\app\outputs\apk```

You can install the app.apk on an android emulator or phone.

## Firebase

### Deploy Firebase rules
```firebase deploy --only firestore:rules```

### Deploy Firebase functions
```firebase deploy --only functions```

### Generate orders.g.dart
flutter pub run build_runner watch 