import * as functions from "firebase-functions";
import * as admin from "firebase-admin";

// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript

admin.initializeApp(functions.config().firebase);

// Function which updates the avg rating from one user everytime he gets a new rating.
export const handleNewRating = functions
  .region("europe-west2")
  .firestore.document("persons/{uid}/rating/{ratingId}")
  .onCreate(async (_change, context) => {
    let counter = 0;
    let ratingSum = 0;

    await admin
      .firestore()
      .collection("persons")
      .doc(context.params.uid)
      .collection("rating")
      .get()
      .then((querySnapshot) =>
        querySnapshot.forEach((doc) => {
          ratingSum += doc.data().rating;
          console.info(ratingSum);
          counter++;
          console.info(counter);
        })
      );

    return admin
      .firestore()
      .collection("persons")
      .doc(context.params.uid)
      .update({
        avgRating:
          counter > 0 ? Math.round((ratingSum * 100) / counter) / 100 : 0,
      });
  });

export const handleHelpCounter = functions
  .region("europe-west2")
  .firestore.document("persons/{uid}/rating/{ratingId}")
  .onCreate(async (_change, context) => {
    await admin
      .firestore()
      .collection("persons")
      .doc(context.params.uid)
      .collection("rating")
      .doc(context.params.ratingId)
      .get()
      .then(async (document) => {
        if (document.data()?.role == "creator") {
          await admin
            .firestore()
            .collection("persons")
            .doc(context.params.uid)
            .update({ helpCounter: admin.firestore.FieldValue.increment(1) });
        }
      });
  });
