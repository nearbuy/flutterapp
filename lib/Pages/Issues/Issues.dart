import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nearbuy/Documents/orders.dart';
import 'package:nearbuy/Documents/persons.dart';
import 'package:nearbuy/Pages/Fallback/Fallback.dart';
import 'package:nearbuy/Pages/SideBar.dart';
import 'package:nearbuy/Services/firestore.dart';
import 'package:provider/provider.dart';

class IssuesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var userProfile = Provider.of<UserProfile>(context);

    if (userProfile == null) return Fallback();

    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(title: Text('Meine Gesuche')),
      body: Center(
        // child: Padding(
        //     padding: EdgeInsets.only(top: 10.0, left: 20.0, bottom: 20.0),
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: creatorsView(userProfile),
            ),
            Expanded(
              flex: 1,
              child: helpersView(userProfile),
            ),
          ],
        ),
      ),
      // ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.pushNamedAndRemoveUntil(
            context, '/profil', ModalRoute.withName('/profil')),
        child: Icon(Icons.home),
      ),
    );
  }
}

StreamBuilder<List<Orders>> helpersView(UserProfile userProfile) {
  return StreamBuilder<List<Orders>>(
    stream: db.streamHelperOrders(userProfile.uid),
    builder: (context, snapshot) {
      var orders = snapshot.data;
      List<Widget> orderWidgets = [];
      if (orders == null || orders.length == 0)
        orderWidgets.add(Padding(
          padding: EdgeInsets.only(left: 20, right: 20),
          child: Text(
              'Du hast momentan keine Gesuche angenommen. Wenn du helfen möchtest, schau doch mal auf der Karte nach wer Hilfe braucht.'),
        ));
      else
        orders.forEach(
          (order) {
            orderWidgets.add(
              orderCard(
                  order.creator.firstName + ' ' + order.creator.name,
                  order.creator.uid,
                  order,
                  'helper',
                  context,
                  order.creator.profilePicture),
            );
          },
        );
      return Card(
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: ListTile(
                leading: Icon(Icons.favorite, color: Colors.blue[900]),
                title: Text(
                  'Für andere erledigen',
                  style: Theme.of(context).textTheme.headline4,
                ),
              ),
            ),
            Expanded(
              flex: 5,
              child: ListView(
                children: orderWidgets,
              ),
            ),
          ],
        ),
      );
    },
  );
}

StreamBuilder<List<Orders>> creatorsView(UserProfile userProfile) {
  return StreamBuilder<List<Orders>>(
    stream: db.streamCreatorOrders(userProfile.uid),
    builder: (context, snapshot) {
      var orders = snapshot.data;
      List<Widget> orderWidgets = [];
      if (orders == null || orders.length == 0)
        orderWidgets.add(
          Padding(
            padding: EdgeInsets.only(left: 20.0, right: 20.0),
            child: Text(
                'Du hast momentan keine Gesuche aufgegeben. Falls du Hilfe brauchst drücke, einfach auf das Plus auf der Karte.'),
          ),
        );
      else {
        orders.forEach(
          (order) {
            orderWidgets.add(
              orderCard(
                  order.helper != null
                      ? order.helper.firstName + ' ' + order.helper.name
                      : 'Noch nicht in Bearbeitung.',
                  order.helper != null ? order.helper.uid : 'nobody',
                  order,
                  'creator',
                  context,
                  order.helper != null ? order.helper.profilePicture : null),
            );
          },
        );
      }
      return Card(
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: ListTile(
                leading: Icon(
                  Icons.person,
                  color: Colors.blue[900],
                ),
                title: Text(
                  'Selbst aufgegeben',
                  style: Theme.of(context).textTheme.headline4,
                ),
              ),
            ),
            Expanded(
              flex: 5,
              child: ListView(
                children: orderWidgets,
              ),
            ),
          ],
        ),
      );
    },
  );
}

InkWell orderCard(name, uid, order, role, context, profilePicture) {
  return InkWell(
    onTap: () => Navigator.pushNamed(context, '/help/detail', arguments: order),
    child: ListTile(
      leading: CircleAvatar(
        backgroundImage:
            profilePicture != null ? NetworkImage(profilePicture) : null,
      ),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            flex: 1,
            child: Text(
              name,
              style: Theme.of(context).textTheme.bodyText2,
            ),
          ),
          Flexible(
            flex: 1,
            child: OrderMenuCreator(order: order, uid: uid, role: role),
          )
        ],
      ),
    ),
  );
}

class OrderMenuCreator extends StatelessWidget {
  final String role;
  final Orders order;
  final String uid;
  OrderMenuCreator(
      {@required this.order, @required this.uid, @required this.role});

  @override
  Widget build(BuildContext context) {
    final handleValueChange = (value) {
      switch (value) {
        case 'profile':
          Navigator.pushNamed(context, '/profileOther', arguments: uid);
          break;
        case 'finish':
          Navigator.pushNamed(context, '/rating/create', arguments: order);
          break;
        case 'delete':
          db.deleteOrder(order.uid);
          break;
        case 'removeHelper':
          order.helper = null;
          db.updateOrder(order);
          break;
        case 'showNumber':
          break;
        default:
          print(value);
      }
    };

    return PopupMenuButton(
      onSelected: (value) => handleValueChange(value),
      itemBuilder: role == 'helper'
          ? (BuildContext context) => [
                PopupMenuItem(
                  child: Text('Profil ansehen'),
                  value: 'profile',
                ),
                PopupMenuItem(
                  child: Text('Abschließen'),
                  value: 'finish',
                ),
                PopupMenuItem(
                  child: Text('Zuweisung entfernen'),
                  value: 'removeHelper',
                ),
                PopupMenuItem(
                  child: Text('Nummer freigeben'),
                  value: 'showNumber',
                ),
              ]
          : order.helper != null
              ? (BuildContext context) => [
                    PopupMenuItem(
                      child: Text('Profil ansehen'),
                      value: 'profile',
                    ),
                    PopupMenuItem(
                      child: Text('Abschließen'),
                      value: 'finish',
                    ),
                    PopupMenuItem(
                      child: Text('Nummer freigeben'),
                      value: 'showNumber',
                    ),
                  ]
              : (BuildContext context) => [
                    PopupMenuItem(
                      child: Text('Löschen'),
                      value: 'delete',
                    ),
                  ],
    );
  }
}
