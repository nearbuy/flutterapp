import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nearbuy/Documents/orders.dart';
import 'package:nearbuy/Documents/persons.dart';
import 'package:nearbuy/Services/firestore.dart';
import 'package:provider/provider.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:sprintf/sprintf.dart';

class FinishIssue extends StatefulWidget {
  final Orders order;

  FinishIssue({Key key, @required this.order}) : super(key: key);

  @override
  _FinishIssueState createState() => _FinishIssueState();
}

class _FinishIssueState extends State<FinishIssue> {
  double ratingValue = 0;
  String comment;

  @override
  Widget build(BuildContext context) {
    UserProfile user = Provider.of<UserProfile>(context);
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Bewertung'),
            InkWell(
              onTap: () => null,
              child: Icon(
                Icons.warning,
                color: Colors.white,
                size: 40,
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(bottom: 50.0),
                    child: buildPayPalHeader(context, user),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 50.0),
                    child: buildRatingSection(context),
                  ),
                  buildCommentSection(),
                ],
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: buildFinishButton(user, context),
    );
  }

  Widget buildPayPalHeader(
    BuildContext context,
    UserProfile user,
  ) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: EdgeInsets.only(bottom: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Icon(Icons.monetization_on),
              Text("Bezahlung über PayPal",
                  style: Theme.of(context).textTheme.headline3),
            ],
          ),
        ),
        buildPayPalMeLink(user, context)
      ],
    );
  }

  Widget buildPayPalMeLink(UserProfile user, BuildContext context) {
    return StreamBuilder<double>(
      stream: db.streamBillSum(widget.order.uid),
      builder: (BuildContext context, AsyncSnapshot<double> snapshot) {
        String link = widget.order.helper.paypalLink != null
            ? sprintf('%s/%s', [widget.order.helper.paypalLink, snapshot.data])
            : 'Kein PaypalMeLink hinterlegt.';
        return Column(
          children: <Widget>[
            Text(link, style: Theme.of(context).textTheme.bodyText2),
          ],
        );
      },
    );
  }

  Column buildRatingSection(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(bottom: 10.0, top: 20.0),
          child: Text("Wie zufrieden warst du?",
              style: Theme.of(context).textTheme.headline3),
        ),
        SmoothStarRating(
          isReadOnly: false,
          size: 50.0,
          starCount: 5,
          color: Colors.yellow,
          borderColor: Colors.yellow,
          allowHalfRating: true,
          spacing: 2.0,
          onRated: (value) {
            ratingValue = value;
          },
        ),
      ],
    );
  }

  TextFormField buildCommentSection() {
    return TextFormField(
      maxLines: 5,
      maxLength: 120,
      decoration: InputDecoration(
        labelText: 'Wie war deine Erfahrung?',
        border: OutlineInputBorder(),
      ),
      onChanged: (val) {
        comment = val;
      },
    );
  }

  FloatingActionButton buildFinishButton(
      UserProfile user, BuildContext context) {
    return FloatingActionButton.extended(
      onPressed: () {
        String role = (widget.order.helper.uid == user.uid &&
                widget.order.status.helperFinishCheck == false)
            ? "helper"
            : "creator";
        String uid = role == 'helper'
            ? widget.order.creator.uid
            : widget.order.helper.uid;
        Person person =
            new Person.initial(user.displayName, user.uid, user.profilePicture);
        Rating rating = new Rating.initial("", ratingValue, role, comment,
            person, Timestamp.fromDate(DateTime.now()));
        db.createRating(uid, rating);
        var updatedOrder = widget.order;
        if (role == 'helper')
          updatedOrder.status.helperFinishCheck = true;
        else {
          updatedOrder.status.helpseekerFinishCheck = true;
        }
        db.updateOrder(updatedOrder);
        Navigator.pushNamedAndRemoveUntil(context, '/issues', (route) => false);
      },
      label: Text('Bewerten'),
      // icon: Icon(Icons.rate_review)
    );
  }
}
