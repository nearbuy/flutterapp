import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nearbuy/Services/auth.dart';

class NavDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: CircleAvatar(
                backgroundColor: Colors.white,
                backgroundImage: AssetImage('assets/images/nearbuy.png')),
            decoration: BoxDecoration(color: Theme.of(context).primaryColor),
          ),
          ListTile(
            leading: Icon(Icons.map),
            title: Text('Karte'),
            onTap: () => {Navigator.pushNamed(context, '/')},
          ),
          ListTile(
            leading: Icon(Icons.verified_user),
            title: Text('Profil'),
            onTap: () => {Navigator.pushNamed(context, '/profil')},
          ),
          ListTile(
            leading: Icon(Icons.add),
            title: Text('Hilfe anfordern'),
            onTap: () => {Navigator.pushNamed(context, '/help')},
          ),
          ListTile(
            leading: Icon(Icons.star),
            title: Text('Nachbarschaftskönige'),
            onTap: () => {Navigator.pushNamed(context, '/kings')},
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Abmelden'),
            onTap: () => {
              authService.signOut(),
              Navigator.pushNamedAndRemoveUntil(context, '/login', (r) => false)
            },
          ),
        ],
      ),
    );
  }
}
