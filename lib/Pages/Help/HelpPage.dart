import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nearbuy/Documents/orders.dart';
import 'package:nearbuy/Documents/persons.dart';
import 'package:nearbuy/Pages/Fallback/Fallback.dart';
import 'package:nearbuy/Services/firestore.dart';
import 'package:provider/provider.dart';

class HelpPage extends StatefulWidget {
  HelpPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  HelpPageState createState() => HelpPageState();
}

class HelpPageState extends State<HelpPage> {
  Orders _order = Orders.defaault();
  List<Product> _products = List();
  UserProfile user;

  // product adding relevant property's
  final nameInputFormController = TextEditingController();
  final descriptionInputFormController = TextEditingController();
  final countInputFormController = TextEditingController();
  final priceCodeInputFormController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    user = Provider.of<UserProfile>(context);

    if (user == null) return Fallback();
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.only(left:15, top: 20, right: 15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                buildNameOrderForm(),
                SizedBox(height: 20),
                buildMaxRangeForm(),
                SizedBox(height: 20),
                buildExpirationTimeForm(),
                SizedBox(height: 20),
                buildProductView(),
                SizedBox(height: 40),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    factoryButton(),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          final formState = _formKey.currentState;
          if (formState.validate()) {
            formState.save();
            setOrderNotInputProperties();
            db.createOrderAndProducts(_products, _order);
            Navigator.pushNamedAndRemoveUntil(context, '/issues', (r) => false);
          } else {
            print("Formular ist nicht gültig");
          }
        },
        label: Text('Gesuch aufgeben'),
      ),
    );
  }

  // =======================================================
  // Section where button is produced
  // =======================================================
  FlatButton factoryButton() {
    return FlatButton(
      textColor: Colors.blue[900],
      onPressed: () => showDialog(
        context: context,
        builder: (_) => buildProductAddDialog(null),
      ),
      child: Row(
        children: [
          Icon(Icons.add),
          Text('PRODUKT HINZUFÜGEN'),
        ],
      ),
    );
  }

  // =================================================
  // Section where all forms of the page Help are buil
  // =================================================
  void setOrderNotInputProperties() {
    List<String> names = user.displayName.contains(" ") ? user.displayName.split(" ") : [user.displayName];
    _order.creationTime = Timestamp.now();
    _order.creator =
        new Creator(names[0], names.length > 1 ? names[1]: '', user.uid, user.profilePicture);
    _order.status = new Status(false, false, Timestamp.now(), Timestamp.now());
  }

  TextFormField buildNameOrderForm() {
    return TextFormField(
      keyboardType: TextInputType.text,
      autocorrect: false,
      decoration: InputDecoration(
        labelText: 'Titel',
        hintText: 'z.B. Einkauf',
        border: OutlineInputBorder(),
      ),
      validator: (value) {
        if (value.isEmpty) {
          return 'Bitte Namen eingeben';
        }
        return null;
      },
      onSaved: (val) {
        setState(() {
          _order.name = val;
        });
      },
    );
  }

  TextFormField buildMaxRangeForm() {
    return TextFormField(
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        labelText: 'Max Reichweite',
        hintText: 'Umkreis in km, zwischen 1 und 100',
        border: OutlineInputBorder(),
      ),
      validator: maxRangeValidator,
      inputFormatters: <TextInputFormatter>[
        WhitelistingTextInputFormatter.digitsOnly,
      ],
      onSaved: (val) {
        setState(() {
          _order.maxRange = int.parse(val);
        });
      },
    );
  }

  String maxRangeValidator(value) {
    num zahl = int.parse(value as String);
    if (zahl > 100) {
      return 'Max Range überschritten!';
    }
    return null;
  }

  TextFormField buildExpirationTimeForm() {
    return TextFormField(
      keyboardType: TextInputType.datetime,
      decoration: InputDecoration(
        labelText: 'Bis wann',
        border: OutlineInputBorder(),
        hintText: 'Fälligkeitsdatum, z.B. 20.07.2020',
      ),
      onSaved: (val) {
        setState(() {
          _order.expirationTime = Timestamp.now();
        });
      },
    );
  }

  // ===================================================
  // Section where everything for dynamic product adding
  // ===================================================
  Widget buildProductView() {
    if (_products.length == 0) {
      return Text('Keine Produkte vorhanden');
    }
    List<Widget> productCards = List();
    _products.forEach((productElement) {
      productCards.add(buildProductCard(productElement));
    });
    return Column(
      children: productCards,
    );
  }

  Widget buildProductCard(Product productElement) {
    return InkWell(
      onTap: () => null,
      child: Card(
        child: Padding(
          padding: EdgeInsets.only(top: 10.0, left: 20.0, bottom: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(
                    flex: 1,
                    child: Text(productElement.name,
                        style: Theme.of(context).textTheme.headline4,
                        textAlign: TextAlign.left),
                  ),
                  Flexible(
                    flex: 1,
                    child: buildProductPopUpMenu(productElement),
                  ),
                ],
              ),
              ListTile(
                contentPadding: EdgeInsets.only(left: 5.0, right: 5.0),
                title: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(bottom: 5.0),
                        child: Text(
                          productElement.description,
                          style: Theme.of(context).textTheme.bodyText2,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 5.0),
                        child: Text(
                          'Anzahl: ' + productElement.count.toString(),
                          style: Theme.of(context).textTheme.bodyText2,
                        ),
                      ),
                      Text(
                        'Preis: ' + productElement.price.toString() + ' €',
                        style: Theme.of(context).textTheme.bodyText2,
                      ),
                    ]),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildProductPopUpMenu(Product productElement) {
    final handleValueChange = (value) {
      switch (value) {
        case 'delete':
          setState(() {
            _products.remove(productElement);
          });
          break;
        case 'edit':
          showDialog(
            context: context,
            builder: (_) => buildProductAddDialog(productElement),
          );
          break;
        default:
          print(value);
      }
    };

    return PopupMenuButton(
      onSelected: (value) => handleValueChange(value),
      itemBuilder: (BuildContext context) => [
        PopupMenuItem(
          child: Text('Bearbeiten'),
          value: 'edit',
        ),
        PopupMenuItem(
          child: Text('Löschen'),
          value: 'delete',
        ),
      ],
    );
  }

  Widget buildProductAddDialog(Product productElement) {
    if (productElement != null) {
      nameInputFormController.text = productElement.name;
      descriptionInputFormController.text = productElement.description;
      countInputFormController.text = productElement.count.toString();
      priceCodeInputFormController.text = productElement.price.toString();
    }

    return AlertDialog(
      title: Text(
          productElement != null ? 'Produkt ändern' : 'Neues Produkt anlegen'),
      content: SingleChildScrollView(
        child: Column(
          children: [
            TextField(
                controller: nameInputFormController,
                decoration: InputDecoration(labelText: 'Name', hintText: 'z.B. Spaghetti',)),
            TextField(
                controller: descriptionInputFormController,
                decoration: InputDecoration(labelText: 'Beschreibung', hintText: 'z.B. Barilla No.3, 1 Pck.', )),
            TextField(
                controller: countInputFormController,
                decoration: InputDecoration(labelText: 'Anzahl', hintText: 'z.B. 1',)),
            TextField(
                controller: priceCodeInputFormController,
                decoration: InputDecoration(labelText: 'Preis', hintText: 'Gesamtpreis, z.B. 2.5',)),
          ],
        ),
      ),
      actions: <Widget>[
        FlatButton(
            onPressed: () => handleButtonAlertDialog(productElement),
            child: Text('SPEICHERN')),
        FlatButton(
            onPressed: () => Navigator.pop(context), child: Text('ABBRECHEN'))
      ],
    );
  }

  void handleButtonAlertDialog(Product productElement) {
    if (productElement == null) {
      setState(() {
        _products.add(new Product(
            int.parse(countInputFormController.text),
            descriptionInputFormController.text,
            nameInputFormController.text,
            double.parse(priceCodeInputFormController.text)));
      });
    } else {
      setState(() {
        _products.remove(productElement);

        productElement.name = nameInputFormController.text;
        productElement.description = descriptionInputFormController.text;
        productElement.count = int.parse(countInputFormController.text);
        productElement.price = double.parse(priceCodeInputFormController.text);
        _products.add(productElement);
      });
    }
    disposeView();
    Navigator.pop(context);
  }

  void disposeView() {
    nameInputFormController.text = "";
    descriptionInputFormController.text = "";
    countInputFormController.text = "";
    priceCodeInputFormController.text = "";
  }
}
