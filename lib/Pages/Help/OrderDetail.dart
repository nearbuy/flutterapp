import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nearbuy/Documents/orders.dart';
import 'package:nearbuy/Documents/persons.dart';
import 'package:nearbuy/Services/firestore.dart';
import 'package:provider/provider.dart';

class OrderDetail extends StatelessWidget {
  final Orders order;

  OrderDetail({Key key, @required this.order}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    UserProfile userProfile = Provider.of<UserProfile>(context);
    Widget buildFloatingActionButton() {
      if (order.helper == null)
        return FloatingActionButton.extended(
          onPressed: () => {
            db.setHelperForIssue(order, userProfile.uid),
            Navigator.pushNamed(context, '/issues')
          },
          label: Text('Malochen!'),
        );
      else if (order.helper.uid == userProfile.uid ||
          order.creator.uid == userProfile.uid) {
        return FloatingActionButton.extended(
          onPressed: () =>
              Navigator.pushNamed(context, '/rating/create', arguments: order),
          label: Text('Abschließen'),
        );
      }
      return null;
    }

    return Scaffold(
        appBar: AppBar(
          title: Text(order.name),
        ),
        body: Padding(
          padding: EdgeInsets.only(left: 0, top: 0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              buildProductInformation(context),
              Expanded(
                child: Card(
                  child: ProductList(uid: order.uid),
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: buildFloatingActionButton());
  }

  // ================================================
  // Section where everything order related is build
  // ================================================
  Widget buildProductInformation(BuildContext context) {
    return SingleChildScrollView(
        child: Card(
      child: Padding(
        padding: EdgeInsets.only(left: 20.0, top: 10.0, bottom: 10.0),
        child: Column(
          children: <Widget>[
            buildCreator(context),
            buildHelper(context),
          ],
        ),
      ),
    ));
  }

  Column buildCreator(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(bottom: 15.0),
          child: Row(
            children: [
              Padding(
                padding: EdgeInsets.only(right: 10.0),
                child: Icon(Icons.person),
              ),
              Text(
                "Empfänger:",
                style: Theme.of(context).textTheme.headline4,
              ),
            ],
          ),
        ),
        ListTile(
          onTap: () => Navigator.pushNamed(context, '/profileOther',
              arguments: order.creator.uid),
          leading: CircleAvatar(
            backgroundImage: order.creator.profilePicture != null
                ? NetworkImage(order.creator.profilePicture)
                : null,
          ),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                order.creator.firstName + ' ' + order.creator.name,
                style: Theme.of(context).textTheme.bodyText2,
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget buildHelper(BuildContext context) {
    var helper = order.helper == null
        ? ListTile(
            title: Text('Noch nicht zugewiesen.',
                style: Theme.of(context).textTheme.bodyText2),
          )
        : ListTile(
            onTap: () => Navigator.pushNamed(context, '/profileOther',
                arguments: order.helper.uid),
            leading: CircleAvatar(
              backgroundImage: order.helper.profilePicture != null
                  ? NetworkImage(order.helper.profilePicture)
                  : null,
            ),
            title: Text(
              order.helper.firstName + ' ' + order.helper.name,
              style: Theme.of(context).textTheme.bodyText2,
            ),
          );

    return Padding(
      padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(bottom: 15.0),
            child: Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(right: 10.0),
                  child: Icon(Icons.person),
                ),
                Text(
                  "Bearbeiter",
                  style: Theme.of(context).textTheme.headline4,
                ),
              ],
            ),
          ),
          helper,
        ],
      ),
    );
  }
}

class ProductList extends StatelessWidget {
  final String uid;

  ProductList({@required this.uid});

  @override
  Widget build(BuildContext context) {
    Widget buildPrice(BuildContext context) {
      return StreamBuilder<double>(
        stream: db.streamBillSum(uid),
        builder: (BuildContext context, AsyncSnapshot<double> snapshot) {
          String base = "Gesamtpreis: ";
          if (snapshot.hasData) {
            return Text(
              base + snapshot.data.toString() + ' €',
              style: Theme.of(context).textTheme.bodyText2,
            );
          }
          return Text(base + "nicht hinterlegt",
              style: Theme.of(context).textTheme.bodyText2);
        },
      );
    }

    return StreamBuilder<List<Product>>(
      stream: db.streamProductsNamesOfOrder(uid),
      builder: (BuildContext context, AsyncSnapshot<List<Product>> snapshot) {
        if (snapshot.data == null)
          return Text('Produkte Laden hat nicht geklappt.');
        List<Entry> products = [];
        snapshot.data.forEach(
          (product) => products.add(
            Entry(
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(product.name,
                        style: Theme.of(context).textTheme.bodyText2),
                    Text(product.price.toString() + ' €',
                        style: Theme.of(context).textTheme.bodyText2)
                  ],
                ),
                <Entry>[
                  Entry(Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(bottom: 10.0),
                          child: Text(product.description,
                              style: Theme.of(context).textTheme.bodyText2),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 10.0),
                          child: Text('Anzahl: ' + product.count.toString(),
                              style: Theme.of(context).textTheme.bodyText2),
                        ),
                      ])),
                ]),
          ),
        );
        if (products.length < 1)
          products.add(
            Entry(
              Text('Keine Produkte vorhanden'),
            ),
          );
        List<Entry> productDropdown = <Entry>[
          Entry(
              Text('Einkaufsliste',
                  style: Theme.of(context).textTheme.headline4),
              products),
          Entry(buildPrice(context))
        ];
        return ListView.builder(
          itemBuilder: (BuildContext context, int index) =>
              EntryItem(productDropdown[index]),
          itemCount: productDropdown.length,
        );
      },
    );
  }
}

class EntryItem extends StatelessWidget {
  const EntryItem(this.entry);

  final Entry entry;

  Widget _buildTiles(Entry root) {
    if (root.children.isEmpty) return ListTile(title: root.title);
    return ExpansionTile(
      key: PageStorageKey<Entry>(root),
      title: root.title,
      children: root.children.map(_buildTiles).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildTiles(entry);
  }
}

class Entry {
  final Widget title;
  final List<Entry> children;

  Entry(this.title, [this.children = const <Entry>[]]);
}
