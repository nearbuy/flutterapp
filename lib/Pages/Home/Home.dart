import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong/latlong.dart';
import 'package:nearbuy/Documents/orders.dart';
import 'package:nearbuy/Services/firestore.dart';

import '../SideBar.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Marker> markers = List();
  MapController mapController = new MapController();

  @override
  Widget build(context) {
    return FutureBuilder<Position>(
        future: getCurrentGeoPostion(),
        builder: (BuildContext context, AsyncSnapshot<Position> snapshot) {
          if (snapshot.hasData) {
            return buildMap(context, snapshot.data);
          } else {
            return Scaffold(
              drawer: NavDrawer(),
              appBar: AppBar(
                title: Text('Home'),
              ),
              body: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
        });
  }

  Scaffold buildMap(BuildContext context, Position position) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: Center(
        child: Stack(
          children: <Widget>[
            new StreamBuilder<List<Orders>>(
                stream: db.streamOrders(),
                builder: (BuildContext context,
                    AsyncSnapshot<List<Orders>> snapshot) {
                  if (snapshot.hasData) {
                    return buildStreamMap(snapshot.data, position, context);
                  }
                  return CircularProgressIndicator();
                }),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.pushNamed(context, '/help'),
        child: Icon(Icons.add),
      ),
    );
  }

  FlutterMap buildStreamMap(
      List<Orders> orders, Position position, BuildContext context) {
    orders.forEach((order) {
      markers.add(factoryMarker(context, order));
    });
    return FlutterMap(
      mapController: mapController,
      options: new MapOptions(
        center: new LatLng(position.latitude, position.longitude),
        zoom: 13.0,
      ),
      layers: [
        new TileLayerOptions(
            urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            subdomains: ['a', 'b', 'c']),
        new MarkerLayerOptions(
          markers: markers,
        ),
      ],
    );
  }

  Marker factoryMarker(BuildContext context, Orders order) {
    return new Marker(
      width: 80.0,
      height: 80.0,
      point: new LatLng(
          order.issueLocation.latitude, order.issueLocation.longitude),
      builder: (ctx) => new Container(
        child: IconButton(
          icon: Icon(Icons.local_offer),
          onPressed: () {
            Navigator.pushNamed(context, '/help/detail', arguments: order);
          },
        ),
      ),
    );
  }

  Future<Position> getCurrentGeoPostion() async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    return position;
  }
}
