import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nearbuy/Documents/persons.dart';
import 'package:nearbuy/Pages/Fallback/Fallback.dart';
import 'package:nearbuy/Pages/SideBar.dart';
import 'package:nearbuy/Services/firestore.dart';
import 'package:provider/provider.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var user = Provider.of<FirebaseUser>(context);
    var loggedIn = user != null;

    if (!loggedIn) return Fallback();

    return Scaffold(
      resizeToAvoidBottomInset: false,
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Text(user.displayName+" (Ich)"),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(left: 10.0, right: 10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Center(
                child: Column(
                  children: <Widget>[
                    UserCard(),
                    AddressCard(),
                    PayPalCard(),
                    PhoneCard(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.pushNamed(context, '/issues'),
        child: Icon(Icons.local_offer),
        tooltip: 'meine Gesuche',
      ),
    );
  }
}

class UserCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var userProfile = Provider.of<UserProfile>(context);

    if (userProfile == null)
      return Center(
          child: Card(
        child: Text('no user...'),
      ));

    // return Center(
    return Container(
      padding: EdgeInsets.only(top: 20.0, bottom: 30.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Flexible(
            flex: 1,
            child: CircleAvatar(
              backgroundImage: NetworkImage(userProfile.profilePicture),
              maxRadius: 60,
              minRadius: 20,
            ),
          ),
          Expanded(
            flex: 2,
            child: Padding(
              padding: EdgeInsets.only(left: 0.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  InkWell(
                    onTap: () => Navigator.pushNamed(context, '/ratings', arguments: userProfile),
                    child: SmoothStarRating(
                        allowHalfRating: true,
                        starCount: 5,
                        rating: userProfile.avgRating,
                        size: 40.0,
                        isReadOnly: true,
                        color: Colors.yellow,
                        borderColor: Colors.yellow,
                        spacing: 0.0),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: Text(
                      'Hat bereits ' +
                          userProfile.helpCounter.toString() +
                          ' Nachbarn geholfen.',
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class AddressCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var userProfile = Provider.of<UserProfile>(context);

    if (userProfile == null)
      return Card(
        child: Text('no user...'),
      );

    return StreamBuilder<List<Address>>(
      stream: db.streamAddress(userProfile.uid),
      builder: (context, snapshot) {
        var addresses = snapshot.data;
        if (addresses == null || addresses.length == 0)
          return ProfileCard(
            headline: 'Default Adresse',
            title: 'Keine Adressen hinterlegt.',
            subtitle: '',
            hint: '',
          );

        var card;
        addresses.forEach((address) => address.isDefault
            ? card = ProfileCard(
                headline: 'Default Adresse',
                title: address.street + ' ' + address.number.toString(),
                subtitle: address.zipCode + ' ' + address.city,
                hint: '')
            : null);
        return card == null
            ? ProfileCard(
                headline: 'Default Adresse',
                title: 'Keine Default Adresse hinterlegt.',
                subtitle: '',
                hint: '',
              )
            : card;
      },
    );
  }
}

class PayPalCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var userProfile = Provider.of<UserProfile>(context);

    if (userProfile == null)
      return Card(
        child: Text('no user...'),
      );

    return ProfileCard(
      headline: 'PayPalMe Link',
      title: userProfile.paypalLink ?? 'kein PayPalMe Link hinterlegt',
      subtitle: '',
      hint: 'z.B. https://paypal.me/olliolive',
    );
  }
}

class PhoneCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var userProfile = Provider.of<UserProfile>(context);

    if (userProfile == null)
      return Card(
        child: Text('no user...'),
      );
    return ProfileCard(
      headline: 'Telefonnummer',
      title: userProfile.phoneNumber ?? 'keine Telefonnummer hinterlegt',
      subtitle: '',
      hint: 'z.B. 07557/123456789',
    );
  }
}

class ProfileCard extends StatelessWidget {
  final String headline;
  final String title;
  final String subtitle;
  final String hint;

  ProfileCard(
      {@required this.headline, @required this.title, @required this.subtitle, @required this.hint});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: headline == 'Default Adresse'
          ? () => Navigator.pushNamed(context, '/address')
          : () => showDialog(
                context: context,
                builder: (_) => ProfilePageForm(type: headline, hint: hint),
              ),
      child: Card(
        child: Padding(
          padding: EdgeInsets.only(top: 10.0, left: 20.0, bottom: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(headline,
                      style: Theme.of(context).textTheme.headline4,
                      textAlign: TextAlign.left),
                  Padding(
                      padding: EdgeInsets.only(right: 10.0),
                      child: Icon(Icons.edit))
                ],
              ),
              ListTile(
                contentPadding: EdgeInsets.only(left: 10.0),
                title:
                    Text(title, style: Theme.of(context).textTheme.bodyText2),
                subtitle: Text(
                  subtitle,
                  style: Theme.of(context).textTheme.bodyText2,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ProfilePageForm extends StatefulWidget {
  final String type;
  final String hint; 

  ProfilePageForm({Key key, @required this.type, @required this.hint}): super(key: key);

  @override
  _ProfilePageFormState createState() => _ProfilePageFormState();
}

class _ProfilePageFormState extends State<ProfilePageForm> {
  final profileInputFormController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the
    // widget tree.
    profileInputFormController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var userProfile = Provider.of<UserProfile>(context);
    var handleDialogChange;
    switch (widget.type) {
      case 'PayPalMe Link':
        profileInputFormController.text = userProfile.paypalLink;
        handleDialogChange = () => {
              userProfile.paypalLink = profileInputFormController.text,
              db.updateUser(userProfile),
              Navigator.pop(context)
            };
        break;
      case 'Telefonnummer':
        profileInputFormController.text = userProfile.phoneNumber;
        handleDialogChange = () => {
              userProfile.phoneNumber = profileInputFormController.text,
              db.updateUser(userProfile),
              Navigator.pop(context)
            };
        break;
      default:
        handleDialogChange = () => {Navigator.pop(context)};
    }

    return AlertDialog(
      title: Text(widget.type + ' ändern'),
      content: TextField(controller: profileInputFormController, decoration: InputDecoration(hintText: widget.hint),),
      actions: <Widget>[
        FlatButton(
            onPressed: () => handleDialogChange(), child: Text('SPEICHERN')),
        FlatButton(
            onPressed: () => Navigator.pop(context), child: Text('ABBRECHEN'))
      ],
    );
  }
}
