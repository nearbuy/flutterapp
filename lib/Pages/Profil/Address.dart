import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nearbuy/Documents/persons.dart';
import 'package:nearbuy/Pages/SideBar.dart';
import 'package:nearbuy/Services/firestore.dart';
import 'package:provider/provider.dart';

class AddressPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var userProfile = Provider.of<UserProfile>(context);

    if (userProfile == null)
      return Card(
        child: Text('no user...'),
      );

    return Scaffold(
      resizeToAvoidBottomInset: false,
      drawer: NavDrawer(),
      appBar: AppBar(title: Text('Meine Adressen')),
      body: SingleChildScrollView(
        child: Center(
          child: StreamProvider<List<Address>>.value(
            value: db.streamAddress(userProfile.uid),
            child: AddressView(),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => showDialog(
          context: context,
          builder: (_) => AddressForm(address: null),
        ),
        child: Icon(Icons.add),
      ),
    );
  }
}

class AddressView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var addresses = Provider.of<List<Address>>(context);
    if (addresses == null)
      return Card(child: Text('Keine Adressen vorhanden.'));
    var cards = List<Widget>();
    addresses.forEach(
      (address) => cards.add(
        AddressCard(address: address),
      ),
    );
    return Column(
      children: cards,
    );
  }
}

class AddressCard extends StatelessWidget {
  final Address address;
  AddressCard({@required this.address});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => null,
      child: Card(
        child: Padding(
          padding: EdgeInsets.only(top: 10.0, left: 20.0, bottom: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(
                    flex: 1,
                    child: Text(
                        address.isDefault ? 'Default Adresse' : 'Adresse',
                        style: Theme.of(context).textTheme.headline4,
                        textAlign: TextAlign.left),
                  ),
                  Flexible(
                    flex: 1,
                    child: AddressMenu(
                      address: address,
                    ),
                  ),
                ],
              ),
              ListTile(
                contentPadding: EdgeInsets.only(left: 5.0, right: 5.0),
                title: Text(address.street + ' ' + address.number.toString(),
                    style: Theme.of(context).textTheme.bodyText2),
                subtitle: Text(
                  address.zipCode + ' ' + address.city,
                  style: Theme.of(context).textTheme.bodyText2,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class AddressMenu extends StatelessWidget {
  final Address address;
  AddressMenu({@required this.address});

  @override
  Widget build(BuildContext context) {
    var addresses = Provider.of<List<Address>>(context);
    var userProfile = Provider.of<UserProfile>(context);

    final handleValueChange = (value) {
      switch (value) {
        case 'main':
          addresses.forEach((a) {
            if (a.id == address.id) {
              a.isDefault = true;
              db.updateUserAddress(userProfile.uid, a);
            } else {
              a.isDefault = false;
              db.updateUserAddress(userProfile.uid, a);
            }
          });
          break;
        case 'delete':
          db.deleteUserAddress(userProfile.uid, address);
          break;
        case 'edit':
          showDialog(
            context: context,
            builder: (_) => AddressForm(address: address),
          );
          break;
        default:
          print(value);
      }
    };

    return PopupMenuButton(
      onSelected: (value) => handleValueChange(value),
      itemBuilder: (BuildContext context) => [
        PopupMenuItem(
          child: Text('Als Hauptadresse verwenden'),
          value: 'main',
        ),
        PopupMenuItem(
          child: Text('Bearbeiten'),
          value: 'edit',
        ),
        PopupMenuItem(
          child: Text('Löschen'),
          value: 'delete',
        ),
      ],
    );
  }
}

class AddressForm extends StatefulWidget {
  final Address address;

  AddressForm({Key key, @required this.address}) : super(key: key);

  @override
  _AddressFormState createState() => _AddressFormState();
}

class _AddressFormState extends State<AddressForm> {
  final streetInputFormController = TextEditingController();
  final numberInputFormController = TextEditingController();
  final cityInputFormController = TextEditingController();
  final zipCodeInputFormController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the
    // widget tree.
    streetInputFormController.dispose();
    numberInputFormController.dispose();
    cityInputFormController.dispose();
    zipCodeInputFormController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var userProfile = Provider.of<UserProfile>(context);

    if (widget.address != null) {
      streetInputFormController.text = widget.address.street;
      numberInputFormController.text = widget.address.number;
      cityInputFormController.text = widget.address.city;
      zipCodeInputFormController.text = widget.address.zipCode;
    }
    var handleDialogChange = () => {
          if (widget.address == null)
            {
              db.createUserAddress(
                  userProfile.uid,
                  Address(
                      street: streetInputFormController.text,
                      number: numberInputFormController.text,
                      city: cityInputFormController.text,
                      zipCode: zipCodeInputFormController.text,
                      isDefault: false))
            }
          else
            {
              widget.address.street = streetInputFormController.text,
              widget.address.number = numberInputFormController.text,
              widget.address.city = cityInputFormController.text,
              widget.address.zipCode = zipCodeInputFormController.text,
              db.updateUserAddress(userProfile.uid, widget.address),
            },
          Navigator.pop(context)
        };

    return AlertDialog(
      title: Text(
          widget.address != null ? 'Adresse ändern' : 'Neue Adresse anlegen'),
      content: SingleChildScrollView(
        child: Column(
          children: [
            TextField(
                controller: streetInputFormController,
                decoration: InputDecoration(labelText: 'Straße', hintText: 'z.B. Moltkestr.')),
            TextField(
                controller: numberInputFormController,
                decoration: InputDecoration(labelText: 'Hausnummer', hintText: 'z.B. 30')),
            TextField(
                controller: cityInputFormController,
                decoration: InputDecoration(labelText: 'Ort', hintText: 'z.B. Karlsruhe' )),
            TextField(
                controller: zipCodeInputFormController,
                decoration: InputDecoration(labelText: 'Postleitzahl', hintText: 'z.B. 76133')),
          ],
        ),
      ),
      actions: <Widget>[
        FlatButton(
            onPressed: () => handleDialogChange(), child: Text('SPEICHERN')),
        FlatButton(
            onPressed: () => Navigator.pop(context), child: Text('ABBRECHEN'))
      ],
    );
  }
}
