import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nearbuy/Documents/persons.dart';
import 'package:nearbuy/Pages/SideBar.dart';
import 'package:nearbuy/Services/firestore.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class ProfileOtherPage extends StatelessWidget {
  final String uid;

  ProfileOtherPage({@required this.uid});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<UserProfile>(
        stream: db.streamUser(uid),
        builder: (context, snapshot) {
          UserProfile userProfile = snapshot.data;
          return Scaffold(
            resizeToAvoidBottomInset: false,
            drawer: NavDrawer(),
            appBar: AppBar(
              title: userProfile == null
                  ? Text('kein User')
                  : Text(userProfile.displayName),
            ),
            body: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.only(left: 10.0, right: 10.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Center(
                      child: Column(
                        children: userProfile == null
                            ? <Widget>[
                                noUser(),
                              ]
                            : <Widget>[
                                userCard(context, userProfile),
                                addressCard(context, userProfile.uid),
                              ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () => Navigator.pop(context),
              child: Icon(Icons.arrow_back),
            ),
          );
        });
  }

  Column noUser() {
    return Column(
      children: [
        Center(
          child: Text('no user...'),
        ),
      ],
    );
  }

  Container userCard(BuildContext context, UserProfile userProfile) {
    return Container(
      padding: EdgeInsets.only(top: 20.0, bottom: 30.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Flexible(
            flex: 1,
            child: CircleAvatar(
              backgroundImage: NetworkImage(userProfile.profilePicture),
              maxRadius: 60,
              minRadius: 20,
            ),
          ),
          Expanded(
            flex: 2,
            child: Padding(
              padding: EdgeInsets.only(left: 0.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  InkWell(
                    onTap: () => Navigator.pushNamed(context, '/ratings',
                        arguments: userProfile),
                    child: SmoothStarRating(
                        allowHalfRating: true,
                        starCount: 5,
                        rating: userProfile.avgRating,
                        size: 30.0,
                        isReadOnly: true,
                        color: Colors.yellow,
                        borderColor: Colors.yellow,
                        spacing: 0.0),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: Text(
                      'Hat bereits ' +
                          userProfile.helpCounter.toString() +
                          ' Nachbarn geholfen.',
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  StreamBuilder<List<Address>> addressCard(BuildContext context, String uid) {
    return StreamBuilder<List<Address>>(
      stream: db.streamAddress(uid),
      builder: (context, snapshot) {
        var addresses = snapshot.data;
        if (addresses == null || addresses.length == 0)
          return profileCard(
            'Adresse',
            'Keine Adressen hinterlegt.',
            '',
            context,
          );

        var card;
        addresses.forEach((address) => address.isDefault
            ? card = profileCard(
                'Default Adresse',
                address.street + ' ' + address.number.toString(),
                address.zipCode + ' ' + address.city,
                context)
            : null);
        return card == null
            ? profileCard(
                'Default Adresse',
                'Keine Adressen hinterlegt.',
                '',
                context,
              )
            : card;
      },
    );
  }

  Card profileCard(headline, title, subtitle, context) {
    return Card(
      child: Padding(
        padding: EdgeInsets.only(top: 10.0, left: 20.0, bottom: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(headline,
                style: Theme.of(context).textTheme.headline4,
                textAlign: TextAlign.left),
            ListTile(
              contentPadding: EdgeInsets.only(left: 10.0),
              title: Text(title, style: Theme.of(context).textTheme.bodyText2),
              subtitle: Text(
                subtitle,
                style: Theme.of(context).textTheme.bodyText2,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
