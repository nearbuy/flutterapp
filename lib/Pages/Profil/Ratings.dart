import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nearbuy/Documents/persons.dart';
import 'package:nearbuy/Pages/SideBar.dart';
import 'package:nearbuy/Services/firestore.dart';
import 'package:provider/provider.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class RatingsPage extends StatelessWidget {
  final UserProfile userProfile;

  RatingsPage({@required this.userProfile});

  @override
  Widget build(BuildContext context) {
    if (userProfile == null)
      return Card(
        child: Text('no user...'),
      );

    return Scaffold(
      resizeToAvoidBottomInset: false,
      drawer: NavDrawer(),
      appBar: AppBar(title: Text('Ratings von ' + userProfile.displayName)),
      body: StreamProvider<List<Rating>>.value(
        value: db.streamRatings(userProfile.uid),
        child: RatingView(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.pop(context),
        child: Icon(Icons.arrow_back),
      ),
    );
  }
}

class RatingView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var ratings = Provider.of<List<Rating>>(context);
    if (ratings == null || ratings.length == 0)
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Text(
              'Noch keine Ratings vorhanden.',
              style: Theme.of(context).textTheme.bodyText2,
            ),
          ),
        ],
      );
    var cards = List<Widget>();
    ratings.forEach(
      (rating) => cards.add(
        RatingCard(rating: rating),
      ),
    );
    return SingleChildScrollView(
      child: Center(
        child: Column(
          children: cards,
        ),
      ),
    );
  }
}

class RatingCard extends StatelessWidget {
  final Rating rating;
  RatingCard({@required this.rating});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        child: Padding(
          padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Flexible(
                flex: 1,
                child: ratingAvatar(),
              ),
              Flexible(
                flex: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(bottom: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Flexible(
                            flex: 1,
                            child: Padding(
                              padding: EdgeInsets.only(right: 0.0),
                              child: Text(
                                rating.person.displayName,
                                textAlign: TextAlign.center,
                                style: Theme.of(context).textTheme.bodyText2,
                              ),
                            ),
                          ),
                          Flexible(
                            flex: 1,
                            child: SmoothStarRating(
                                allowHalfRating: true,
                                starCount: 5,
                                rating: rating.rating,
                                size: 20.0,
                                isReadOnly: true,
                                color: Colors.yellow,
                                borderColor: Colors.yellow,
                                spacing: 0.0),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 20),
                      child: Text(
                        rating.text != null ? '"' + rating.text + '"' : "",
                        style: TextStyle(fontStyle: FontStyle.italic),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  CircleAvatar ratingAvatar() {
    return CircleAvatar(
      backgroundImage: rating.person.profilePicture != null
          ? NetworkImage(rating.person.profilePicture)
          : null,
      maxRadius: 30,
      minRadius: 30,
    );
  }
}
