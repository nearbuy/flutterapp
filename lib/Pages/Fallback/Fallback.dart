import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Fallback extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sign Out'),
      ),
      body: Center(
          child: MaterialButton(
          onPressed: () => Navigator.pushNamed(context, 'login'),
          color: Theme.of(context).cardColor,
          textColor: Theme.of(context).primaryColor,
          child: Text('Zum Login'))
      ),
    );
  }
}