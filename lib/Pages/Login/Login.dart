import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nearbuy/Services/auth.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Willkommen bei NearBuy!'),
        centerTitle: true ,
      ),
      body: Container(
        color: Colors.white,
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[Image(image: AssetImage('assets/images/nearbuy.png')),SizedBox(height: 50),LoginButton()
              ],
          ),

      ),

      )
    );
  }
}

class LoginButton extends StatefulWidget {
  LoginButton({Key key}): super(key:key);
  
  @override
  LoginButtonState createState() => LoginButtonState();
}

class LoginButtonState extends State<LoginButton> {
  bool _loading = false;
  

  @override
  Widget build(BuildContext context) {
    void signIn() async {
      setState(() => _loading = true);
      await authService.googleSignIn();
      Navigator.pushNamedAndRemoveUntil(context, '/', (r) => false);
      setState(() => _loading = false);
    }

    if (_loading)
      return CircularProgressIndicator();
    else
      return OutlineButton(
        splashColor: Colors.grey,
          onPressed: () => signIn(),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
          highlightElevation: 0,
        borderSide: BorderSide(color: Colors.grey),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image(image: AssetImage("assets/images/googlelogo.png"), height: 35.0),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  'Sign in with Google',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.grey,
                  ),
                ),
              )
            ],
          ),
        ),
      );
  }
}