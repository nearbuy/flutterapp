import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

part 'orders.g.dart';

Timestamp _stampFromJson(int val) => Timestamp.fromMillisecondsSinceEpoch(val);
int _stampToJson(Timestamp val) => val.millisecondsSinceEpoch;

@JsonSerializable(explicitToJson: true)
class Orders {
  @JsonKey(ignore: true)
  String uid;
  @JsonKey(fromJson: _stampFromJson, toJson: _stampToJson)
  Timestamp creationTime;
  Creator creator;
  @JsonKey(fromJson: _stampFromJson, toJson: _stampToJson)
  Timestamp expirationTime;
  Helper helper;
  IssueLocation issueLocation;
  int maxRange;
  Status status;
  String name;

  Orders(this.creationTime, this.creator, this.expirationTime, this.helper,
      this.issueLocation, this.maxRange, this.status, this.name);

  Orders.withId(this.uid, this.creationTime, this.creator, this.expirationTime,
      this.helper, this.issueLocation, this.maxRange, this.status, this.name);

  Orders.defaault();

  factory Orders.fromFirestore(DocumentSnapshot docSnapshot) {
    if (!docSnapshot.exists) {
      return null;
    }

    Map data = docSnapshot.data;
    Orders result = _$OrdersFromJson(data);
    result.uid = docSnapshot.documentID;
    return result;
  }

  factory Orders.fromJson(Map<String, dynamic> json) => _$OrdersFromJson(json);

  Map<String, dynamic> toJson() => _$OrdersToJson(this);
}

// ================================================
// Fields of document orders, which are objects
// ================================================
@JsonSerializable()
class Creator {
  String firstName;
  String name;
  String uid;
  String profilePicture;

  Creator(this.firstName, this.name, this.uid,
      this.profilePicture);

  factory Creator.fromMap(Map data) {
    return Creator.fromJson(data);
  }

  factory Creator.fromJson(Map<String, dynamic> json) =>
      _$CreatorFromJson(json);

  Map<String, dynamic> toJson() => _$CreatorToJson(this);
}

@JsonSerializable()
class Helper {
  String firstName;
  String name;
  String uid;
  String paypalLink;
  String profilePicture;

  Helper(this.firstName, this.name, this.uid, this.paypalLink, this.profilePicture);

  factory Helper.fromMap(Map data) {
    return Helper.fromJson(data);
  }

  factory Helper.fromJson(Map<String, dynamic> json) => _$HelperFromJson(json);

  Map<String, dynamic> toJson() => _$HelperToJson(this);
}

@JsonSerializable()
class IssueLocation {
  double latitude;
  double longitude;

  IssueLocation(this.latitude, this.longitude);

  factory IssueLocation.fromMap(Map data) {
    return IssueLocation(data['latitude'], data['longitude']);
  }

  factory IssueLocation.fromJson(Map<String, dynamic> json) =>
      _$IssueLocationFromJson(json);

  Map<String, dynamic> toJson() => _$IssueLocationToJson(this);
}

@JsonSerializable()
class Status {
  bool helperFinishCheck;
  bool helpseekerFinishCheck;
  @JsonKey(fromJson: _stampFromJson, toJson: _stampToJson)
  Timestamp timeStampHelper;
  @JsonKey(fromJson: _stampFromJson, toJson: _stampToJson)
  Timestamp timeStampHelpseeker;

  Status(this.helperFinishCheck, this.helpseekerFinishCheck,
      this.timeStampHelper, this.timeStampHelpseeker);

  Status.inital(this.helperFinishCheck, this.helpseekerFinishCheck);

  factory Status.fromMap(Map data) {
    return Status(data['HelperFinishCheck'], data['HelpseekerFinishCheck'],
        data['TimeStampHelper'], data['TimeStampHelpseeker']);
  }

  factory Status.fromJson(Map<String, dynamic> json) => _$StatusFromJson(json);

  Map<String, dynamic> toJson() => _$StatusToJson(this);
}

// ===================================
// Subcollections of collection orders
// ===================================
@JsonSerializable()
class Bill {
  @JsonKey(ignore: true)
  String id;
  String picture;
  String shopName;
  double sum;

  Bill(this.picture, this.shopName, this.sum);

  Bill.withId(this.id, this.picture, this.shopName, this.sum);

  factory Bill.fromFirestore(DocumentSnapshot docSnapshot) {
    if (!docSnapshot.exists) {
      return null;
    }

    Map data = docSnapshot.data;
    Bill result = _$BillFromJson(data);
    result.id = docSnapshot.documentID;
    return result;
  }

  factory Bill.fromJson(Map<String, dynamic> json) => _$BillFromJson(json);

  Map<String, dynamic> toJson() => _$BillToJson(this);
}

@JsonSerializable()
class Product {
  @JsonKey(ignore: true)
  String id;
  int count;
  String description;
  String name;
  double price;

  Product(this.count, this.description, this.name, this.price);

  Product.justName(this.name);

  Product.defaault();

  Product.withId(this.id, this.count, this.description, this.name, this.price);

  factory Product.fromFirestore(DocumentSnapshot docSnapshot) {
    if (!docSnapshot.exists) {
      return null;
    }

    Map data = docSnapshot.data;
    Product result = _$ProductFromJson(data);
    result.id = docSnapshot.documentID;
    return result;
  }

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);

  Map<String, dynamic> toJson() => _$ProductToJson(this);
}
