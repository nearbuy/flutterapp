import 'package:cloud_firestore/cloud_firestore.dart';

class UserProfile {
  final String uid;
  final String displayName;
  Timestamp birthday;
  String email;
  String phoneNumber;
  int helpCounter;
  String paypalLink;
  String profilePicture;
  double avgRating;

  UserProfile(
      {this.uid,
      this.displayName,
      this.birthday,
      this.email,
      this.phoneNumber,
      this.helpCounter,
      this.paypalLink,
      this.profilePicture,
      this.avgRating});

  factory UserProfile.fromFirestore(DocumentSnapshot doc) {
    if (!doc.exists) return null;

    Map data = doc.data;

    return UserProfile(
        uid: doc.documentID,
        displayName: data['displayName'] ?? null,
        birthday: data['birthday'] ?? null,
        email: data['email'] ?? null,
        phoneNumber: data['phoneNumber'] ?? null,
        helpCounter: data['helpCounter'] ?? 0,
        paypalLink: data['paypalLink'] ?? null,
        profilePicture: data['profilePicture'] ?? null,
        avgRating: (data['avgRating'] != null)
            ? double.parse(data['avgRating'].toString())
            : 0.0);
  }
}

class Address {
  final String id;
  String city;
  String number;
  String street;
  String zipCode;
  bool isDefault;

  Address(
      {this.id,
      this.city,
      this.number,
      this.street,
      this.zipCode,
      this.isDefault});

  factory Address.fromFirestore(DocumentSnapshot doc) {
    if (!doc.exists) return null;

    Map data = doc.data;

    return Address(
      id: doc.documentID,
      city: data['city'] ?? null,
      number: data['number'] ?? null,
      street: data['street'] ?? null,
      zipCode: data['zipCode'] ?? null,
      isDefault: data['main'] ?? false,
    );
  }
}

class Rating {
  final String id;
  final double rating;
  final String role;
  final String text;
  final Person person;
  final Timestamp created;

  Rating(
      {this.id, this.rating, this.role, this.text, this.person, this.created});

  Rating.initial(
      this.id, this.rating, this.role, this.text, this.person, this.created);

  factory Rating.fromFirestore(DocumentSnapshot doc) {
    if (!doc.exists) return null;

    Map data = doc.data;

    return Rating(
      id: doc.documentID,
      rating: (data['rating'] != null)
          ? double.parse(data['rating'].toString())
          : 0.0,
      role: data['role'] ?? null,
      text: data['text'] ?? null,
      person: Person.fromMap(data['person']) ?? null,
      created: data['created'] ?? null,
    );
  }
}

class Person {
  final String displayName;
  final String uid;
  final String profilePicture;

  Person({this.displayName, this.uid, this.profilePicture});

  Person.initial(
      this.displayName, this.uid, this.profilePicture);

  factory Person.fromMap(Map data) {
    return Person(
        displayName: data['displayName'] ?? null,
        uid: data['uid'] ?? null,
        profilePicture: data['profilePicture'] ?? null,
    );
  }
}
