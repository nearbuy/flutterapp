// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'orders.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Orders _$OrdersFromJson(Map<String, dynamic> json) {
  return Orders(
    _stampFromJson(json['creationTime'] as int),
    json['creator'] == null
        ? null
        : Creator.fromJson(json['creator'] as Map<String, dynamic>),
    _stampFromJson(json['expirationTime'] as int),
    json['helper'] == null
        ? null
        : Helper.fromJson(json['helper'] as Map<String, dynamic>),
    json['issueLocation'] == null
        ? null
        : IssueLocation.fromJson(json['issueLocation'] as Map<String, dynamic>),
    json['maxRange'] as int,
    json['status'] == null
        ? null
        : Status.fromJson(json['status'] as Map<String, dynamic>),
    json['name'] as String,
  );
}

Map<String, dynamic> _$OrdersToJson(Orders instance) => <String, dynamic>{
      'creationTime': _stampToJson(instance.creationTime),
      'creator': instance.creator?.toJson(),
      'expirationTime': _stampToJson(instance.expirationTime),
      'helper': instance.helper?.toJson(),
      'issueLocation': instance.issueLocation?.toJson(),
      'maxRange': instance.maxRange,
      'status': instance.status?.toJson(),
      'name': instance.name,
    };

Creator _$CreatorFromJson(Map<String, dynamic> json) {
  return Creator(
    json['firstName'] as String,
    json['name'] as String,
    json['uid'] as String,
    json['profilePicture'] as String,
  );
}

Map<String, dynamic> _$CreatorToJson(Creator instance) => <String, dynamic>{
      'firstName': instance.firstName,
      'name': instance.name,
      'uid': instance.uid,
      'profilePicture': instance.profilePicture,
    };

Helper _$HelperFromJson(Map<String, dynamic> json) {
  return Helper(
    json['firstName'] as String,
    json['name'] as String,
    json['uid'] as String,
    json['paypalLink'] as String,
    json['profilePicture'] as String,
  );
}

Map<String, dynamic> _$HelperToJson(Helper instance) => <String, dynamic>{
      'firstName': instance.firstName,
      'name': instance.name,
      'uid': instance.uid,
      'paypalLink': instance.paypalLink,
      'profilePicture': instance.profilePicture,
    };

IssueLocation _$IssueLocationFromJson(Map<String, dynamic> json) {
  return IssueLocation(
    (json['latitude'] as num)?.toDouble(),
    (json['longitude'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$IssueLocationToJson(IssueLocation instance) =>
    <String, dynamic>{
      'latitude': instance.latitude,
      'longitude': instance.longitude,
    };

Status _$StatusFromJson(Map<String, dynamic> json) {
  return Status(
    json['helperFinishCheck'] as bool,
    json['helpseekerFinishCheck'] as bool,
    _stampFromJson(json['timeStampHelper'] as int),
    _stampFromJson(json['timeStampHelpseeker'] as int),
  );
}

Map<String, dynamic> _$StatusToJson(Status instance) => <String, dynamic>{
      'helperFinishCheck': instance.helperFinishCheck,
      'helpseekerFinishCheck': instance.helpseekerFinishCheck,
      'timeStampHelper': _stampToJson(instance.timeStampHelper),
      'timeStampHelpseeker': _stampToJson(instance.timeStampHelpseeker),
    };

Bill _$BillFromJson(Map<String, dynamic> json) {
  return Bill(
    json['picture'] as String,
    json['shopName'] as String,
    (json['sum'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$BillToJson(Bill instance) => <String, dynamic>{
      'picture': instance.picture,
      'shopName': instance.shopName,
      'sum': instance.sum,
    };

Product _$ProductFromJson(Map<String, dynamic> json) {
  return Product(
    json['count'] as int,
    json['description'] as String,
    json['name'] as String,
    (json['price'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      'count': instance.count,
      'description': instance.description,
      'name': instance.name,
      'price': instance.price,
    };
