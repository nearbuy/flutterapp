import 'package:flutter/material.dart';
import 'package:nearbuy/Pages/Help/HelpPage.dart';
import 'package:nearbuy/Pages/Help/OrderDetail.dart';
import 'package:nearbuy/Pages/Home/Home.dart';
import 'package:nearbuy/Pages/Issues/FinishIssue.dart';
import 'package:nearbuy/Pages/Issues/Issues.dart';
import 'package:nearbuy/Pages/Kings/Kings.dart';
import 'package:nearbuy/Pages/Login/Login.dart';
import 'package:nearbuy/Pages/Profil/Address.dart';
import 'package:nearbuy/Pages/Profil/Profil.dart';
import 'package:nearbuy/Pages/Profil/ProfileOther.dart';
import 'package:nearbuy/Pages/Profil/Ratings.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => HomePage());
      case '/help':
        return MaterialPageRoute(builder: (_) => HelpPage(title: 'Hilfe anfordern'));
      case '/help/detail':
        return MaterialPageRoute(builder: (_) => OrderDetail(order: args));
      case '/kings':
        return MaterialPageRoute(builder: (_) => KingsPage());
      case '/profil':
        return MaterialPageRoute(builder: (_) => ProfilePage());
      case '/profileOther':
        return MaterialPageRoute(builder: (_) => ProfileOtherPage(uid: args,));
      case '/login':
        return MaterialPageRoute(builder: (_) => LoginPage());
      case '/issues':
        return MaterialPageRoute(builder: (_) => IssuesPage());
      case '/ratings':
        return MaterialPageRoute(builder: (_) => RatingsPage(userProfile: args,));
      case '/rating/create':
        return MaterialPageRoute(builder: (_) => FinishIssue(order: args));
      case '/address':
        return MaterialPageRoute(builder: (_) => AddressPage());
      default:
        return errorRoute();
    }
  }

  static Route<dynamic> errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
          appBar: AppBar(title: Text('NearBuy')),
          body: Center(child: Text('Something went wrong ...')));
    });
  }
}
