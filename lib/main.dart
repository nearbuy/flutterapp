import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:nearbuy/Routes/routeGenerator.dart';
import 'package:provider/provider.dart';

import 'Pages/Home/Home.dart';
import 'Pages/Login/Login.dart';
import 'Services/firestore.dart';

void main() => runApp(NearBuyProvider());

class NearBuyProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        StreamProvider<FirebaseUser>.value(
            value: FirebaseAuth.instance.onAuthStateChanged),
      ],
      child: NearBuy(),
    );
  }
}

class NearBuy extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var user = Provider.of<FirebaseUser>(context);
    var isLoggedIn = user != null;

    return StreamProvider.value(
      value: user != null ? db.streamUser(user.uid): null,
      child: MaterialApp(
        title: 'NearBuy',
        // Theme to specify the colors and other style attributes globally
        // Colors will also be used by standard components
        theme: ThemeData(
            // maybe make it possible to switch between lite and dark theme
            brightness: Brightness.light,
            primaryColor: Colors.blue[800],
            accentColor: Colors.cyan[600],
            cardColor: Colors.white,

            // maybe use Google Fonts https://pub.dev/packages/google_fonts or another font
            fontFamily: 'Roboto',

            // specify the text style globally
            textTheme: TextTheme(
                headline1:
                    TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
                headline2:
                    TextStyle(fontSize: 54.0, fontWeight: FontWeight.bold),
                headline3: TextStyle(
                    fontSize: 28.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.blue[900]),
                headline4: TextStyle(fontSize: 24, fontWeight: FontWeight.bold,
                    color: Colors.blue[900]),
                bodyText2: TextStyle(fontSize: 20.0, fontFamily: 'Roboto'),
                bodyText1: TextStyle(fontSize: 18.0, fontFamily: 'Roboto'),
                button: TextStyle(
                    fontSize: 18, fontFamily: 'Roboto', color: Colors.white)),
            iconTheme: IconThemeData(color: Colors.blue[900], size: 30)),
        home: isLoggedIn ? HomePage() : LoginPage(),
        onGenerateRoute: RouteGenerator.generateRoute,
      ),
    );
  }
}
