import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:geolocator/geolocator.dart';
import 'package:nearbuy/Documents/orders.dart';
import 'package:nearbuy/Documents/persons.dart';

// perform Firestore access (CRUD)
class FirestoreService {
  final Firestore _db = Firestore.instance;

  // ==============================
  // Operations for document persons
  // ===============================

  // get the document
  Future<UserProfile> getUser(String id) async {
    var snap = await _db.collection('persons').document(id).get();

    return UserProfile.fromFirestore(snap);
  }

  // get a stream of the document, subscribes to changes
  Stream<UserProfile> streamUser(String id) {
    return _db
        .collection('persons')
        .document(id)
        .snapshots()
        .map((snap) => UserProfile.fromFirestore(snap));
  }

  // get a list of Addresses from one user
  Stream<List<Address>> streamAddress(String uid) {
    var ref = _db
        .collection('persons')
        .document(uid)
        .collection('address')
        .orderBy('city');

    return ref.snapshots().map((list) =>
        list.documents.map((doc) => Address.fromFirestore(doc)).toList());
  }

  // get a list of Ratings from one user
  Stream<List<Rating>> streamRatings(String uid) {
    var ref = _db.collection('persons').document(uid).collection('rating').orderBy('created', descending: true);

    return ref.snapshots().map((list) =>
        list.documents.map((doc) => Rating.fromFirestore(doc)).toList());
  }

  // ==============================
  // Operations for document orders
  // ===============================
  Stream<List<Orders>> streamOrders() {
    return _db
        .collection('orders')
        .where("helper", isNull: true)
        .snapshots()
        .map((collection) => collection.documents
            .map((json) => Orders.fromFirestore(json))
            .toList());
  }

  Stream<List<Product>> streamProductsNamesOfOrder(String orderId) {
    return _db
        .collection('orders')
        .document(orderId)
        .collection('product')
        .snapshots()
        .map((collection) => collection.documents
            .map((json) => Product.fromFirestore(json))
            .toList());
  }

  Stream<double> streamBillSum(String orderId) {
    return _db
        .collection('orders')
        .document(orderId)
        .collection('product')
        .snapshots()
        .map((collection) => collection.documents
            .map((json) => Product.fromFirestore(json))
            .map((product) => product.price)
            .toList().reduce((a, b) => a + b));
  }

  Stream<List<Orders>> streamHelperOrders(String uid) {
    return _db
        .collection('orders')
        .where('helper.uid', isEqualTo: uid)
        .where('status.helperFinishCheck', isEqualTo: false)
        .snapshots()
        .map((collection) => collection.documents
            .map((doc) => Orders.fromFirestore(doc))
            .toList());
  }

  Stream<List<Orders>> streamCreatorOrders(String uid) {
    return _db
        .collection('orders')
        .where('creator.uid', isEqualTo: uid)
        .where('status.helpseekerFinishCheck', isEqualTo: false)
        .snapshots()
        .map((collection) => collection.documents
            .map((doc) => Orders.fromFirestore(doc))
            .toList());
  }

  Future<void> setHelperForIssue(Orders order, String helperId) async {
    UserProfile user = await getUser(helperId);
    List<String> names = user.displayName.contains(" ") ? user.displayName.split(" ") : [user.displayName];
    
      order.helper = new Helper(
        names[0],
        names.length > 1 ? names[1] : '',
        user.uid,
        user.paypalLink,
        user.profilePicture,
      );

      DocumentReference orderRef = _db.collection('orders').document(order.uid);
      await orderRef.setData(order.toJson(), merge: true);
  }

  Future<void> createOrderAndProducts(
      List<Product> products, Orders order) async {
    Position postion = await getCurrentGeoPostion();
    order.issueLocation =
        new IssueLocation(postion.latitude, postion.longitude);

    DocumentReference orderRef =
        await _db.collection('orders').add(order.toJson());

    products.forEach((product) async {
      await _db
          .collection('orders')
          .document(orderRef.documentID)
          .collection('product')
          .add(product.toJson());
    });
  }

  Future<void> createRating(String userId, Rating rating) async {
    await _db.collection('persons').document(userId).collection('rating').add(
      {
        'rating': rating.rating,
        'role': rating.role,
        'text': rating.text,
        'person': {
          'displayName': rating.person.displayName,
          'uid': rating.person.uid,
          'profilePicture': rating.person.profilePicture,
        },
        'created': rating.created,
      },
    );
  }

  Future<Position> getCurrentGeoPostion() async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    return position;
  }

  // ==============================
  // Operations for persisting mock data
  // ===============================
  Future<void> createOrdersMock() async {
    Orders order = new Orders(
        Timestamp.now(),
        new Creator("Leo", "Fehler", "schwanz", null),
        Timestamp.now(),
        null,
        new IssueLocation(37.41883, -122.085801),
        80,
        new Status(false, true, Timestamp.now(), Timestamp.now()),
        "Helfen 1");
    Bill bill = new Bill("/path/try/yourself", "EDEKA", 200.5);
    Product spaetzle = new Product(2, "Spätzle", "Spätzle 505", 5.0);
    Product pizza = new Product(2, "Pizza", "Pizza", 15.0);

    Orders order2 = new Orders(
        Timestamp.now(),
        new Creator("Leo", "Fehler", "schwanz", null),
        Timestamp.now(),
        null,
        new IssueLocation(37.428099, -122.0827497),
        80,
        new Status(false, true, Timestamp.now(), Timestamp.now()),
        "Helfen 2");

    Orders order3 = new Orders(
        Timestamp.now(),
        new Creator("Leo", "Fehler", "schwanz", null),
        Timestamp.now(),
        null,
        new IssueLocation(37.416886, -122.0817627),
        80,
        new Status(false, true, Timestamp.now(), Timestamp.now()),
        "Helfen 2");

    DocumentReference docuReference =
        await _db.collection('orders').add(order.toJson());

    await _db
        .collection('orders')
        .document(docuReference.documentID)
        .collection('bill')
        .add(bill.toJson());

    await _db
        .collection('orders')
        .document(docuReference.documentID)
        .collection('product')
        .add(spaetzle.toJson());
    await _db
        .collection('orders')
        .document(docuReference.documentID)
        .collection('product')
        .add(pizza.toJson());

    await _db.collection('orders').add(order2.toJson());
    await _db.collection('orders').add(order3.toJson());
  }

  // Create a new Person
  Future<void> createUser(FirebaseUser user) async {
    DocumentReference ref = _db.collection('persons').document(user.uid);

    return ref.setData({
      'avgRating': 0.0,
      'birthday': null,
      'displayName': user.displayName,
      'email': user.email,
      'helpCounter': 0,
      'paypalLink': null,
      'profilePicture': user.photoUrl,
      'phoneNumber': user.phoneNumber,
    }, merge: true);
  }

  Future<void> updateUser(UserProfile userprofile) async {
    DocumentReference ref = _db.collection('persons').document(userprofile.uid);

    return ref.setData({
      'avgRating': userprofile.avgRating,
      'birthday': userprofile.birthday,
      'displayName': userprofile.displayName,
      'email': userprofile.email,
      'helpCounter': userprofile.helpCounter,
      'paypalLink': userprofile.paypalLink,
      'profilePicture': userprofile.profilePicture,
      'phoneNumber': userprofile.phoneNumber,
    }, merge: true);
  }

  Future<void> createUserAddress(String uid, Address address) async {
    DocumentReference ref = _db
        .collection('persons')
        .document(uid)
        .collection('address')
        .document();

    return ref.setData(
      {
        'city': address.city,
        'number': address.number,
        'street': address.street,
        'zipCode': address.zipCode,
        'main': address.isDefault,
      },
    );
  }

  Future<void> updateUserAddress(String uid, Address address) async {
    DocumentReference ref = _db
        .collection('persons')
        .document(uid)
        .collection('address')
        .document(address.id);

    return ref.setData({
      'city': address.city,
      'number': address.number,
      'street': address.street,
      'zipCode': address.zipCode,
      'main': address.isDefault,
    }, merge: true);
  }

  Future<void> createUserRating(String uid, Rating rating) async {
    DocumentReference ref =
        _db.collection('persons').document(uid).collection('rating').document();

    return ref.setData(
      {
        'rating': rating.rating,
        'role': rating.role,
        'text': rating.text,
        'person': {
          'displayName': rating.person.displayName,
          'uid': rating.person.uid,
        }
      },
    );
  }

  updateOrder(Orders order) async {
    DocumentReference orderRef = _db.collection("orders").document(order.uid);
    await orderRef.setData(order.toJson(), merge: true);
  }

  Future<void> deleteOrder(String orderId) async {
    return _db.collection('orders').document(orderId).delete();
  }

  Future<void> deleteUserAddress(String uid, Address address) async {
    DocumentReference ref = _db
        .collection('persons')
        .document(uid)
        .collection('address')
        .document(address.id);

    return ref.delete();
  }
}

final FirestoreService db = FirestoreService();
