import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:nearbuy/Services/firestore.dart';

class AuthService extends ChangeNotifier {
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;

    Future<void> googleSignIn() async {
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
        idToken: googleAuth.idToken, accessToken: googleAuth.accessToken);


    final FirebaseUser user =
        (await _auth.signInWithCredential(credential)).user;
    

    db.getUser(user.uid).then((userProfile) {
      if (userProfile == null) db.createUser(user);
    }, onError: (e) {
      handleError(e);
    }).catchError(handleError);
  }

  void handleError(e) {
    print('Error with user creation in firestore occured');
    print(e);
    }
  
  void signOut() {
    _auth.signOut();
  }
}

final AuthService authService = AuthService();
